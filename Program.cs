﻿using System;

namespace If_else
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Benvenuto popolo,nel mio programma di sterminio carino ");

            int ancora;

            do
            {
                int scelta;
                double numeroUno;
                double numeroDue;

                Console.Write("Inserisci il valore del primo numero: ");
                numeroUno = Convert.ToInt32(Console.ReadLine());
                Console.Write("Inserisci ora il valore del secondo numero: ");
                numeroDue = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ora dimmi mio prode guerriero,che operazione vorresti fare??????  ");
                Console.WriteLine(" 1 - somma ");
                Console.WriteLine(" 2 - sottrazione ");
                Console.WriteLine(" 3 - moltiplicazione ");
                Console.WriteLine(" 4 - divisione ");
                scelta = Convert.ToInt32(Console.ReadLine());

                switch (scelta) //la domanda è sempre "quanto vale quello che ho tra parentesi?"
                {
                    case 1: //somma
                        Console.WriteLine("Il risultato è: " + numeroUno + numeroDue);
                        break;
                    case 2: //differenza
                        Console.WriteLine("Il risultato è: " + (numeroUno - numeroDue));
                        break;
                    case 3: //moltiplicazione
                        Console.WriteLine("Il risultato è: " + numeroUno * numeroDue);
                        break;
                    case 4: //divisione
                        Console.WriteLine("Il risultato è: " + numeroUno / numeroDue);
                        break;

                        //stesso codice condiviso da due casi
                    case 5: 
                    case 6: 
                        Console.WriteLine("Il numero da te inserito è una cagata");
                        break;

                    default:
                        Console.WriteLine("Il numero da te inserito è una cagata doppia");
                        break;
                }

                //version con gli if
                //if (scelta == 1)
                //{
                //    Console.WriteLine("Il risultato è: " + numeroUno + numeroDue);
                //}
                //else if (scelta == 2)
                //{
                //    Console.WriteLine("Il risultato è: " + (numeroUno - numeroDue));
                //}
                //else if (scelta == 3)
                //{
                //    Console.WriteLine("Il risultato è: " + numeroUno * numeroDue);
                //}
                //else if (scelta == 4)
                //{
                //    Console.WriteLine("Il risultato è: " + numeroUno / numeroDue);
                //}
                //else
                //{
                //    Console.WriteLine("Il numero da te inserito è una cagata");
                //}

                Console.WriteLine("Premi uno per uscire altrimenti riparti");
                ancora = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
            }
            while (ancora != 1); //ancora è diverso da 1? Vero = si ; Falso = no


            //altri cicli
            while (ancora != 1)
            {
                //bla bla
            }

            for (int i = 0; i < 10; i = i + 1)
            {
                //inializzo il contatore (solo al primo giro) --> i = 0

                //controllo la condizione (i è minore di 10?)
                //si:
                //eseguo le istruzioni
                //incremento il contatore
                //si ritorna su (al controllo)
                //no:
                //esco dal ciclo
            }
        }
    }
}
